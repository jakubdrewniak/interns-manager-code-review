import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  // dla jednolinijkowej templatki możesz ją zrobić inline, nie ma potrzeby robić oddzielnego pliku
  styleUrls: ['./app.component.css']
  // zbędny plik styli
})
export class AppComponent {}

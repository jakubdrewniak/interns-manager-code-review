import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AddInternComponent} from "./shared/components/interns/add/add-intern.component";
import {EditInternComponent} from "./shared/components/interns/edit/edit-intern.component";
import {ListInternComponent} from "./shared/components/interns/list/list-intern.component";
import {UserResolver} from "./shared/resolvers/UserResolver";
import {UserDetailsResolver} from "./shared/resolvers/UserDetailsResolver";

const routes: Routes = [
  {
    path: '', component: ListInternComponent, resolve: {
      userModel: UserResolver,
    },
  },
  {path: 'add/user', component: AddInternComponent},
  // jaki jest sens tego patha? bo jeśli jest 'add/user' to ja rozumiem, że będą inne ścieżki,
  // typu 'add/something'. imo lepiej by było grupować te routy z głównym routem 'user',
  // a kolejne miały by postać 'user/add', 'user/edit/:id', 'user/:id'; dla listy moze zostać,
  // ale gdyby miała się zmienić strona główna, a lista internów byłaby podstroną, to mogłaby mieć
  // path 'user/list' albo 'users'
  {
    path: 'edit/:id/user', component: EditInternComponent, resolve: {
      userDetail: UserDetailsResolver
    }
  },
  {path: '', redirectTo: '/', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

import {Component} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {Subject, Subscription} from "rxjs";
import {UserModel} from "../../../models/User.model";
import {IUser} from "../../../interfaces/IUser";
import {UserDTO} from "../../../dtos/UserDTO";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-list-intern',
  templateUrl: 'list-intern.component.html',
  styleUrls: ['list-intern.component.css']
})
export class ListInternComponent{
  // brak deklaracji hooków: export class ListInternComponent implements OnInit, OnDestroy
  private ngUnsubscribe = new Subject<UserDTO>();
  // subject uzyty jedynie do udsybskrybowania? to nie ma sensu.
  getUsers!: UserModel;
   // niepoprawna nazwa, pole ktore ma zawierać dane ma nazwę jak dla metody (bo ma czasownik 'get')
  getListUsers: any;
  // błąd jak wyzej, no i pole w ogole nie jest uzywane
  findUsers: any;
  // czemu any? tu pasuje string

  constructor(private userService: UserService, private route: ActivatedRoute) {
  }

  ngOnInit(): UserModel {
    // ngOnInit nie ma prawa nic zwracać, jej return type to void
    this.getUsers = this.route.snapshot.data['userModel'];

    return this.getUsers;
    // jak wyżej, nie ma sensu nic tutaj zwracać
  }

  ngOnDestroy() {
    this.ngUnsubscribe.unsubscribe();
    // brak subskrypcji od której można się odsubskrubować
  }

  paginateDataByPage(page: any): Subscription {
    // myląca nazwa, lepiej by było coś w stylu 'loadPage'  albo 'loadPageByNumber'
    return this.userService.getUsers(page).subscribe((value: UserModel) => this.getUsers = value);
  }

  goToPage(page: any): Subscription | null {
    // mysle ze tę metode i metodę paginateDataByPage można by skleić w jedną
    if (page >= 1 && page <= this.getUsers.total_pages) {
      return this.userService.getUsers(page).subscribe((value: UserModel) => this.getUsers = value);
    }

    return null;
    // po co zwracać null? lepiej zablokować przycisk next jeśli nie ma kolejnej strony. to da lepszy UX
  }

  userByNameOrSurname(index: number, user: IUser) {
    return user.first_name && user.last_name;
  }
}

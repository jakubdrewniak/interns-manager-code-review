import {Component, Input} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subject} from "rxjs";
import {UserDTO} from "../../../dtos/UserDTO";

@Component({
  selector: 'app-form-intern',
  templateUrl: './form-intern.component.html',
  styleUrls: ['./form-intern.component.css']
})
export class FormInternComponent {
  // uwagi jak dla listy
  private ngUnsubscribe = new Subject<UserDTO>();
  // uwagi jak dla listy
  validateForm!: FormGroup;
  // czemu validateForm? to po prostu form
  @Input() getUserData: any;
  imgUrl: string | ArrayBuffer | null = '';
  getListUsers: any;
  // nieuzywane pole
  formData: any = UserDTO;
  // wartością formData jest klasa? totalnie nie rozumiem zamysłu. btw powinienieś zmienić w tsconfigu
  // zeby nie mozna bylo deklarować 'any' (Albo w linterze)

  constructor(private userService: UserService, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    if (this.getUserData) {
      let getData = this.getUserData;
      let getDataUser = getData.data;

      this.formData = {
        first_name: getDataUser.first_name,
        last_name: getDataUser.last_name,
        avatar: getDataUser.avatar || this.imgUrl,
      }

      this.validateForm = this.formBuilder.group({
        first_name: [this.formData.first_name, [Validators.required]],
        last_name: [this.formData.last_name, [Validators.required]],
        avatar: [this.formData.avatar, [Validators.required]],
      });

      this.imgUrl = getDataUser.avatar || '';
    } else {
      this.validateForm = this.formBuilder.group({
        first_name: [this.formData.first_name, [Validators.required]],
        last_name: [this.formData.last_name, [Validators.required]],
        avatar: [this.formData.avatar, [Validators.required]],
      });
      // zduplikowany kod this.validateForm...
    }
  }

  ngOnDestroy() {
    this.ngUnsubscribe.unsubscribe();
  }

  uploadImageThumbnail(event: any): void {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = () => {
      this.imgUrl = reader.result;
    };
    reader.readAsDataURL(file);
  }

  selectedPhoto(): void {
    const fileInput = document.getElementById('avatar') as HTMLInputElement;
    // angular ma ViewChild, nie ma potrzeby uzywac document.getElementById
    fileInput.click();
  }

  alert(message: string) {
    alert(message)
  }
}

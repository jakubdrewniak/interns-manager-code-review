import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UserDTO} from "../../../dtos/UserDTO";

@Component({
  selector: 'app-edit-intern',
  templateUrl: './edit-intern.component.html',
  styleUrls: ['./edit-intern.component.css']
})
export class EditInternComponent {
  getUserData: any;

  constructor(private readonly route: ActivatedRoute) {
  }

  ngOnInit(): UserDTO {
    this.getUserData = this.route.snapshot.data['userDetail'];

    return this.getUserData;
  }
}

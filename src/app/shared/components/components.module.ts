import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AddInternComponent} from "./interns/add/add-intern.component";
import {EditInternComponent} from "./interns/edit/edit-intern.component";
import {FormInternComponent} from "./interns/form-intern/form-intern.component";
import {ListInternComponent} from "./interns/list/list-intern.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {SearchPipe} from "../pipes/search.pipe";
// ogólna uwaga do folderu 'shared' - shared traci sens jeśli cala apka siedzi w folderze shared, który czyni go tak
// naprawdę folderem core. sugerowałbym dodawać do shared tylko to czego jesteś pewien że będzie dzielone.
// Dodatkowo, jeśli komponenty są faktycznie shared, to niech nie będą w jednym module- po co później importować taki
// jeden, ciężko moduł, jeśli będziesz np uzywal tylko jednego komponentu? rozważ dzielenie komponentów na oddzielne moduły
// (o ile mają faktycznie być dzielone), no i w takiej sytuacji warto skorzystać ze standalone components
@NgModule({
  declarations: [
    ListInternComponent,
    AddInternComponent,
    EditInternComponent,
    FormInternComponent,
    SearchPipe,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    CommonModule
  ]
})
export class ComponentsModule {
}

import {IUser} from "./IUser";
// myląca nazwa interfejsu
export interface IConfigUser {
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
  data: IUser[];
}

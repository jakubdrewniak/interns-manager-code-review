import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserResolver} from "./UserResolver";
import {UserDetailsResolver} from "./UserDetailsResolver";
// nie widzę powodu dla wydzielania resolverów do oddzielnego modułu, w tej sytuacji mogą być tak samo jak
// user service- providedIn: root. za tym już przemawiają zalety.
@NgModule({
  providers: [UserResolver ,UserDetailsResolver ],
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ResolversModule { }

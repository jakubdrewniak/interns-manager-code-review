import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {UserService} from "../services/user.service";
import {UserDTO} from "../dtos/UserDTO";
// nazwa pliku niezgodna z konwencją. user-details.resolver.ts
@Injectable()
export class UserDetailsResolver implements Resolve<UserDTO> {
  constructor(private userService: UserService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UserDTO> | Promise<UserDTO> | UserDTO {
    // zbędny parametr state. jeśli wiesz jaki jest return type, to go zawęź, nie trzeba podawać wszystkich możliwych-
    // a wiesz jaki bedzie return type bo userService.getUser go deklaruje
    let userId = route.params['id'];

    return this.userService.getUser(userId);
  }
}

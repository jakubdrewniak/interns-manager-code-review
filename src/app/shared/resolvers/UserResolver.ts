import {Injectable} from "@angular/core";
import {UserModel} from "../models/User.model";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {UserService} from "../services/user.service";

@Injectable()
export class UserResolver implements Resolve<UserModel> {
  // myląca nazwa resolvera- sugeruje ze pobierasz jednego usera, a to tak naprawdę lista userów.
  // nazwa pliku nie trzyma się przyjętej konwencji- powinna być users-list.resolver.ts
  constructor(private userService: UserService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UserModel> | Promise<UserModel> | UserModel {
    // zbędne parametry route i state. warto by było z nich skorzystać gdybyś faktycznie
    // w url miał numer strony i pobierał dla userów dla tej konkretnej strony.
    return this.userService.getUsers(1);
  }
}

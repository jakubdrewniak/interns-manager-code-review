import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// totalnie zbędny moduł. user service ma providedIn root, więc nie potrzebuje modułu.
@NgModule({
  providers: [ ],
  declarations: [],
  imports: [
    CommonModule,
  ]
})
export class ServicesModule { }

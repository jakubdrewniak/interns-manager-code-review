import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from "../../../environments/environment.development";
import {Observable} from "rxjs";
import {UserModel} from "../models/User.model";
import {UserDTO} from "../dtos/UserDTO";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  getUsers(page: 1): Observable<UserModel> {
    // parametr page ma typ '1'? nie ma to sensu- zmien na typ number (polecam) albo
    // wywal parametr i hardkoduj (raczej nie polecam, ale bedzie to mialo wiecej sensu)
    return this.http.get<UserModel>(`${environment.url}/users?page=${page}`);
  }

  getUser(user: number): Observable<UserDTO> {
    // nie jest to najlepsza nazwa na parametr, lepsza byłaby userId albo id
    return this.http.get<UserDTO>(`${environment.url}/users/${user}`);
  }
}

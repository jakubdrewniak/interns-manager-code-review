import {IUser} from "../interfaces/IUser";
import {IConfigUser} from "../interfaces/IConfigUser";

export class UserModel implements IConfigUser {
  // inicjalizacja poniższych pól nie ma sensu bo i tak będą nadpisane w konstruktorze. nazwa dosyć myląca.
  // nie widzę też powodu żeby tn obiekt był klasą- interfejs/ typ by wystarczył... chociaż na ten moment
  // UserModel robi za alias dla IConfigUser
  data: IUser[] = [];
  page: number = 0;
  per_page: number = 0;
  total: number = 0;
  total_pages: number = 0;

  constructor(data: IUser[], page: number, per_page: number, total: number, total_pages: number) {
    this.data = data;
    this.page = page;
    this.per_page = per_page;
    this.total = total;
    this.total_pages = total_pages;
  }
}

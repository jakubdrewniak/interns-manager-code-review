export class UserDTO {
  // UserDTO wygląda na okrojony IUser. jesli faktycznie potrebujesz interfejsu dla tak okrojonego IUser
  // to wystarczyłby typ Pick<IUser, 'first_name' | 'last_name' | 'avatat'>, ewentualnie na odwrót- stwórz nowy typ
  // poprzez usunięcie pól z IUser za pomocą Omit<...>. i tutaj też nie widzę powodu dla którego to musi byc klasa,
  // interfesj/ typ bylby wystarczający
  data: {
    first_name: string;
    last_name: string;
    avatar: string;
  }

  constructor(data: { first_name: string; last_name: string; avatar: string }) {
    this.data = data;
  }
}

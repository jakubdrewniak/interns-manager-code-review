import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {AppRoutingModule} from "./app-routing.module";
// dodaj jakiś linter, bo formatowanie importów jest niespójne (spacje)
import {NgbAlertModule, NgbModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {ComponentsModule} from "./shared/components/components.module";
import {ServicesModule} from "./shared/services/services.module";
import {HttpClientModule} from "@angular/common/http";
import {ResolversModule} from "./shared/resolvers/resolvers.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    ResolversModule,
    ComponentsModule,
    ServicesModule,
    AppRoutingModule,
    NgbModule,
    NgbPaginationModule,
    NgbAlertModule,
    HttpClientModule,
    FontAwesomeModule
  ],
  providers: [
    HttpClientModule
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
